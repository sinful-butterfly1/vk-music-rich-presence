# vk-music-rich-presence
A python script that displays which track you're currently listening to as your status on Discord. The script should work on all platforms macOS, Linux and Windows.


# Screenshots

![1.png](1.png "First screenshot")

![2.png](2.png "Second screenshot")

![3.png](3.png "Third screenshot")


# Requirements
- python 3 and pip
- python-requests (**python3-requests** on debian-based distros and ***python-requests** on arch-based distros)
- Beautifulsoup4: **pip(3) install bs4 --user**
- pypresence: **pip(3) install pypresence --user**

# Usage (Linux and macOS)

First, make the script executable, like so:
```chmod +x vk-rp```

Then install the dependencies, if it is not already done see requirements.

Last but not least, run it(it should already be an executable, if it isn't: chmod +x vk-rp):

```./vk-rp --username yourVkUsername```

# Usage Windows 

Install dependencies and then run it

```python vk-rp --username yourVkUsername```
